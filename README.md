# Bexio Go Client

[![GoDoc](https://godoc.org/gitlab.com/nxt/public/bexio?status.svg)](https://godoc.org/gitlab.com/nxt/public/bexio)
[![Pipeline Status](https://gitlab.com/nxt/public/bexio/badges/master/pipeline.svg)](https://gitlab.com/nxt/public/bexio/commits/master)

This is a client to consume the [Bexio v2 API][bexio-api] using Go.

[Bexio][bexio] is a Swiss service for accounting and customer relationship management.

This implementation is in no way complete and it is driven by what we need.
Feel free to create a MR if you like to add a feature.

[bexio]: https://bexio.com
[bexio-api]: https://docs.bexio.com/legacy/index.html

## Quick Usage

This is a quick example on how to use the library.
[Read the full documentation on godoc.org][godoc-bexio].

```go
package main

import (
    "fmt"

    "gitlab.com/nxt/public/bexio"
)

func main() {
    api_token := bexio.APIToken{
                    CompanyID:    "abcdef",
                    UserID:       1,
                    PublicKey:    "abcdef",
                    SignatureKey: "abcdef",
                }
    client := bexio.NewClient(api_token)

    allContacts, err := client.ListContacts()
    if err != nil {
        return
    }
    for _, contact := range allContacts {
        fmt.Printf("%s %s", contact.FirstName, contact.LastName)
	}
}
```

[godoc-bexio]: https://godoc.org/gitlab.com/nxt/public/bexio

## Sample projects

You can have a look at two projects that use this library as examples:

* _[o2b][o2b]_ is a tool to import contacts of CSV files exported in Outlook CSV format into Bexio.
* _[b2csv][b2csv]_ is a tool to export certain contacts from Bexio into a CSV format.

[o2b]: https://gitlab.com/nxt/public/o2b
[b2csv]: https://gitlab.com/nxt/public/b2csv

## License

This code is made available to anyone under the conditions of the MIT license.

## About

This repository is currently maintained and funded by [nxt][nxt].

[nxt]: https://nxt.engineering
