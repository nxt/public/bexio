package bexio

// ContactType is a number that designates whether
// a contact is a company or a person
type ContactType int

const (
	// ContactTypeCompany indicates that a contact is a company
	ContactTypeCompany ContactType = 1

	// ContactTypePerson indicates that a contact is a person
	ContactTypePerson ContactType = 2
)

// Contact contains all relevant fields on a contact.
// Note that "LastName" is the company name
// and that "FirstName" is the company additional name
// if ContactTypeID is 1,
type Contact struct {
	ID          int         `json:"id" csv:"-"`
	ContactType ContactType `json:"contact_type_id" csv:"-"`
	LastName    string      `json:"name_1" csv:"Last Name"`
	FirstName   string      `json:"name_2,omitempty" csv:"First Name"`
	OwnerID     int         `json:"owner_id" csv:"-"`
	UserID      int         `json:"user_id" csv:"-"`
	Email       string      `json:"mail,omitempty" csv:"E-mail Address"`
	Address     string      `json:"address" csv:"-"`
	Postcode    string      `json:"postcode" csv:"-"`
	City        string      `json:"city" csv:"-"`
}

// ContactRelation is the relation between two contacts
type ContactRelation struct {
	ContactID    int    `json:"contact_id"`
	ContactSubID int    `json:"contact_sub_id"`
	Description  string `json:"description"`
}
