package bexio

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/go-resty/resty/v2"
)

// APIToken holds any information that is required to
// authenticate with the Bexio API
type APIToken struct {
	CompanyID    string
	UserID       int
	PublicKey    string
	SignatureKey string
}

// Client holds any information that is required to
// send HTTP requests to the Bexio API.
type Client struct {
	restyClient *resty.Client
}

// NewClient initializes a new Bexio.Client
func NewClient(token APIToken) Client {
	return Client{
		restyClient: newRestyClient(token),
	}
}

// newRestyClient creates a new client that is set up to talk to Bexio.
// It intercepts requests in order to add the required
// 'signature' header.
func newRestyClient(token APIToken) *resty.Client {
	client := resty.New()

	//client.SetDebug(true)

	client.SetBaseURL(
		fmt.Sprintf("https://office.bexio.com/api2.php/%s/%d/%s/",
			token.CompanyID,
			token.UserID,
			token.PublicKey),
	)
	client.SetHeader("Accept", "application/json")
	client.SetHeader("User-Agent", "nxt.engineering CSV Contact Exporter")

	client.OnBeforeRequest(func(client *resty.Client, request *resty.Request) error {
		var bodyInput string

		if request.Body != nil {
			// manually marshall body so that it is a string and not an arbitrary interface{} type
			newBody, err := json.Marshal(request.Body)
			if err != nil {
				return err
			}
			bodyInput = string(newBody)
			request.Body = bodyInput
		} else {
			bodyInput = ""
		}

		signatureInput :=
			fmt.Sprintf(
				"%s%s%s%s%s",
				strings.ToLower(request.Method),
				client.HostURL,
				request.URL,
				bodyInput,
				token.SignatureKey,
			)

		signature :=
			fmt.Sprintf("%x", // convert bytes to hex string
				md5.Sum( // calculate the sum
					[]byte( // convert the string to bytes
						signatureInput,
					),
				),
			)

		//log.Printf("The signature for '%s' is '%s'.", signatureInput, signature)

		request.SetHeader("Signature", signature)
		return nil
	},
	)

	return client
}
