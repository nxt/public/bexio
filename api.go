package bexio

import (
	"encoding/json"
	"fmt"
)

// SearchContactRelation calls /contact_relation/search with the given arguments
func (c *Client) SearchContactRelation(filter map[string]interface{}) ([]ContactRelation, error) {
	requestBody := make([]map[string]interface{}, len(filter))

	i := 0
	for k, v := range filter {
		requestBody[i] = map[string]interface{}{
			"field": k,
			"value": v,
		}
		i++
	}

	client := c.restyClient
	client.SetHeader("Content-Type", "application/json")

	response, err := client.R().SetBody(requestBody).Post("/contact_relation/search")
	if err != nil {
		return nil, err
	}
	if response.StatusCode() != 200 {
		return nil, fmt.Errorf(
			"the search for '%s' was unsuccessful as the response status is '%s' (Code: %d')",
			filter,
			response.Status(),
			response.StatusCode(),
		)
	}

	var contactRelations []ContactRelation
	err = json.Unmarshal(response.Body(), &contactRelations)

	return contactRelations, err
}

// ListContacts calls /contact/
func (c *Client) ListContacts() ([]Contact, error) {
	response, err := c.restyClient.R().Get("/contact")
	if err != nil {
		return nil, err
	}
	if response.StatusCode() != 200 {
		return nil, fmt.Errorf(
			"listing the contacts was unsuccessful as the response status is '%s' (Code: %d')",
			response.Status(),
			response.StatusCode(),
		)
	}

	var contacts []Contact
	err = json.Unmarshal(response.Body(), &contacts)

	return contacts, err
}

// ShowContact calls /contact/{id}
func (c *Client) ShowContact(contactID int) (contact Contact, err error) {
	response, err := c.restyClient.R().Get(fmt.Sprintf("/contact/%d", contactID))

	if err != nil {
		return contact, err
	}
	if response.StatusCode() != 200 {
		return contact, fmt.Errorf(
			"listing the contact was unsuccessful as the response status is '%s' (Code: %d')",
			response.Status(),
			response.StatusCode(),
		)
	}

	err = json.Unmarshal(response.Body(), &contact)

	return contact, err
}

// SearchContacts calls /contact/search with the given arguments
func (c *Client) SearchContacts(filter map[string]interface{}) ([]Contact, error) {
	requestBody := make([]map[string]interface{}, len(filter))

	i := 0
	for k, v := range filter {
		requestBody[i] = map[string]interface{}{
			"field": k,
			"value": v,
		}
		i++
	}

	client := c.restyClient
	client.SetHeader("Content-Type", "application/json")

	response, err := client.R().SetBody(requestBody).Post("/contact/search")
	if err != nil {
		return nil, err
	}
	if response.StatusCode() != 200 {
		return nil, fmt.Errorf(
			"the search for '%s' was unsuccessful as the response status is '%s' (Code: %d')",
			filter,
			response.Status(),
			response.StatusCode(),
		)
	}

	var contacts []Contact
	err = json.Unmarshal(response.Body(), &contacts)

	return contacts, err
}

// CreateContact creates the given contact in Bexio
func (c *Client) CreateContact(contact Contact) error {
	err := c.CheckContact(contact)
	if err != nil {
		return err
	}

	response, err := c.restyClient.R().SetBody(contact).Post("/contact")
	if err != nil {
		return err
	}
	if response.StatusCode() != 201 {
		return fmt.Errorf(
			"contact with err-mail '%s' was likely not created as the response status is '%s' (Code: %d')",
			contact.Email,
			response.Status(),
			response.StatusCode(),
		)
	}

	return nil
}

// CheckContact returns an error if the Contact - as is - doesn't fulfil
// the minimum requirements to create it on Bexio
func (c *Client) CheckContact(contact Contact) error {
	if !(contact.ContactType == ContactTypePerson || contact.ContactType == ContactTypeCompany) {
		return fmt.Errorf("contact Type ID must be either %d or %d", ContactTypeCompany, ContactTypePerson)
	}
	if contact.UserID == 0 {
		return fmt.Errorf("the UserID must not be 0")
	}
	if contact.OwnerID == 0 {
		return fmt.Errorf("the OwnerID must not be 0")
	}
	if contact.LastName == "" {
		return fmt.Errorf("the LastName must not be empty")
	}
	return nil
}
